package com.app.test.controller;


import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.app.test.dto.LoginInfo;
import com.app.test.dto.User;
import com.app.test.service.UserService;

@Controller
@RequestMapping(value = "/")
public class UserController {
	@Autowired
	private UserService service;
	@PostMapping(value = "/userinput")
	public ModelAndView signupDuty(User user) {
		try {
			service.saveUserInformation(user);
			System.out.println(user);
		} catch (Exception e) {
			System.out.println("error in save");
		}
		return new ModelAndView("Login.jsp","name","Regestration Successful! Login Now");
	}
	@GetMapping(value = "/userinfo")
	public ModelAndView loginDuty(LoginInfo info) {
		User user = service.getUserDetails(info.getEmail(), info.getPassword());
		FileOutputStream fos=null;
		try {
			Blob image = user.getImage();
			byte[] bytes = image.getBytes(1, (int)image.length());
			fos=new FileOutputStream("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\SoftwareMangement\\pics\\springimg.jpg");
			fos.write(bytes);
			fos.flush();
			System.out.println("Write completed");
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				fos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new ModelAndView("Welcome.jsp","user",user);
	}
}
