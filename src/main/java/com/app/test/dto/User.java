package com.app.test.dto;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Entity
@Table(name="user_details")
public class User implements Serializable{

		/**
	 * 
	 */
	private static final long serialVersionUID = 5860820026566204851L;
		@Id
		@GenericGenerator(name = "gen", strategy = "increment")
		@GeneratedValue(generator = "gen")
		@Column(name = "id")
		private Long id;
		@Column(name = "first_name")
		private String first_name;
		@Column(name = "last_name")
		private String last_name;
		@Column(name = "email")
		private String email;
		@Column(name = "birthday")
		private String birthday;
		@Column(name = "password")
		private String password;
		@Column(name = "gender")
		private String gender;
		@Transient
		private CommonsMultipartFile img;
		@Column(name="image")
		@Lob
		private Blob image;
		public User() {
			// TODO Auto-generated constructor stub
		}
		
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getFirst_name() {
			return first_name;
		}
		public void setFirst_name(String first_name) {
			this.first_name = first_name;
		}
		public String getLast_name() {
			return last_name;
		}
		public void setLast_name(String last_name) {
			this.last_name = last_name;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getBirthday() {
			return birthday;
		}
		public void setBirthday(String birthday) {
			this.birthday = birthday;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		@Override
		public String toString() {
			return "User [id=" + id + ", first_name=" + first_name + ", last_name=" + last_name + ", email=" + email
					+ ", birthday=" + birthday + ", password=" + password + ", gender=" + gender + ", img=" + img + "]";
		}

		public CommonsMultipartFile getImg() {
			return img;
		}

		public void setImg(CommonsMultipartFile img) {
			this.img = img;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}

		public Blob getImage() {
			return image;
		}

		public void setImage(Blob image) {
			this.image = image;
		}

		
}
