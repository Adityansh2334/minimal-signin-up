package com.app.test.dao;

import java.sql.Blob;

import javax.sql.rowset.serial.SerialBlob;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.app.test.dto.User;

@Repository
public class UserDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public void saveUserDataInDb(User user) {
		Session session=null;
		try {
			session=sessionFactory.openSession();
			Transaction beginTransaction = session.beginTransaction();
			CommonsMultipartFile img = user.getImg();
			byte[] bytes = img.getBytes();
			Blob blob = new SerialBlob(bytes);
			user.setImage(blob);
			session.save(user);
			beginTransaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
	}
	@SuppressWarnings("unchecked")
	public User getUserDetailsByEmailPass(String email, String password) {
		Session session=null;
		User user=null;
		try {
			session=sessionFactory.openSession();
			String hql="from "+User.class.getName()+" where email=:mail and password=:pass";
			Query<User> createQuery = session.createQuery(hql);
			createQuery.setParameter("mail", email).setParameter("pass", password);
			user=createQuery.uniqueResult();
			System.out.println("Done Retrive");
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		return user;
	}
	
}
