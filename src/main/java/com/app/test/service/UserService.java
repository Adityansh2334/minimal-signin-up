package com.app.test.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.test.dao.UserDao;
import com.app.test.dto.User;

@Service
public class UserService {
	@Autowired	
	private UserDao dao;
	
	public void saveUserInformation(User user) {
		dao.saveUserDataInDb(user);
	}
	public User getUserDetails(String email, String password) {
		return dao.getUserDetailsByEmailPass(email, password);
	}
	
}
