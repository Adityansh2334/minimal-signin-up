<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.css">
    <title>Login</title>
    <style>
        body{
            background-image: url(https://wallpapercave.com/wp/wp2939946.jpg);
            background-size: cover;
            color: aliceblue;
            text-shadow: 2px 2px 3px rgb(100, 189, 107);
        }
        h1{
            color: rgb(191, 210, 226);
            text-shadow: 2px 2px 3px rgb(209, 107, 158);
        }
        .container{
            display: block;
            background-color: rgba(67, 143, 173, 0.438);
            border: 4px;
            box-shadow: 2px 2px 6px rgb(145, 49, 49);
            border-radius: 2%;
            margin-top: 60px;
            height: 500px;
            width: 300px;
            margin-left: 5%;
        }
        .btn{
            background-color: rgb(160, 30, 90);
            border-color: yellow;
        }
        .btn:hover{
            background-color: yellow;
            color: rgb(165, 55, 55);
        }
        a{
            color: springgreen;
        }
        a:hover{
            color: cornflowerblue;
        }
        img{
            width: 50px;
            height: 50px;
            border-radius: 50%;
        }
    </style>
</head>
<body>
    <div class="container">
    	<h5>${name }</h5>
        <h1 align="center">Welcome <img src="https://www.fbi.gov/image-repository/fbi-seal.jpg/@@images/image" alt="dp"></h1>
        <br>
        <form action="userinfo" method="get">
            <div class="form-group">
              <label for="exampleInputEmail1">Email address</label>
              <input type="text" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
              <small id="emailHelp" class="form-text text-muted"><span style="color: whitesmoke;">We'll never share your email with anyone else.</span></small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Submit</button>
            
            <p>If your are not SignedUp <a href="Regestration.jsp">SignUp Now</a></p>
            <br>
            <p align="center">&COPY;FBI</p>
          </form>
    </div>
</body>
</html>